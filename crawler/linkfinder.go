package crawler

import (
	"io"
	"strings"
	"golang.org/x/net/html"
)

/*
FindAllLinks
------------
This method takes an http body and checks through all its tokens to make a list of all accepted URLs found in anchor tokens
that have a key href. Having found an href it trims of any page refs (#), then if its a relative link reattaches the prefix
url. Next if it has a '/' at the end it removes it to stop the possibility of checking a web page twice. Finally it calls
check on the url and if it comes back false it adds it to an array of urls to be returned.
------------
Input      |
htmlBody : The html body to be analyzed.
prefixURL : The domain url to be used to turn relative links back into absolute urls
------------
Output     |
links : An array of absolute urls that passed all the checks and are formatted correctly
 */
func FindAllLinks(htmlBody io.Reader, prefixUrl string) []string {
	page := html.NewTokenizer(htmlBody)
	var links []string

	for {
		tokenType := page.Next()
		if tokenType == html.ErrorToken {
			return links
		}
		token := page.Token()
		if tokenType == html.StartTagToken && token.DataAtom.String() == "a" {
			for _, attribute := range token.Attr {
				if attribute.Key == "href" {
					url := trimHashtag(attribute.Val)
					if strings.HasPrefix(url, "/" ){
						url = prefixUrl + url
					}
					if strings.HasSuffix(url, "/" ){
						url = url[:len(url)-1]
					}
					if !check(links, url) {
						links = append(links, url)
					}
				}
			}
		}
	}
	return links
}

// trimHashtag removes the page ref (everything after and including '#') from the given url so as not to accidentally check a website multiple times
func trimHashtag(url string) string {
	if strings.Contains(url, "#") {
		return url[:strings.Index(url,"#")]
	}
	return url
}

/*
check
-----
Checks url;
		- Is not empty
		- Is not email-protection, found some websites have these for email links.
		- Starts with http or www (thereby allowing https as-well), stops mailto: etc.
		- Not found in foundURL array, to stop duplicates.
-----
Input|
foundURLS : Array of accepted url strings already found on the page.
urlToCheck : The url string to check against all the conditions.
-----
Output|
shouldNotBeAdded : True if url should not be added to foundURLs array.
 */
func check(foundURLs []string, urlToCheck string) bool {
	var shouldNotBeAdded bool
	if urlToCheck != "" && !strings.Contains(urlToCheck, "email-protection"){
		if strings.HasPrefix(urlToCheck, "http") || strings.HasPrefix(urlToCheck, "www") {
			for _, str := range foundURLs {
				if str == urlToCheck {
					shouldNotBeAdded = true
					break
				}
			}
		} else {
			shouldNotBeAdded = true
		}
	} else {
		shouldNotBeAdded = true
	}
	return shouldNotBeAdded
}

