package crawler

import (
	"testing"
	"strings"
	"fmt"
)

func TestTrimHashtag(t *testing.T) {
	testString := trimHashtag("test#shouldbegone")
	if strings.HasSuffix(testString, "#shouldbegone"){
		fmt.Println("TestTrimHashtag| Test #Suffix removed - Failed")
		t.Errorf("Returned string was:"+testString)
	} else {
		fmt.Println("TestTrimHashtag| Test #Suffix removed - Passed")
	}
}

func TestCheck(t *testing.T){
	testFoundURLs := []string{"https://monzo.com","https://monzo.com/blog", "www.monzo.com"}
	if check(testFoundURLs, "https://monzo.com"){
		fmt.Println("TestCheck|Test Positive http state - passed")
	} else {
		t.Errorf("TestCheck|Test Positive http state - failed")
	}
	if check(testFoundURLs, "www.monzo.com"){
		fmt.Println("TestCheck|Test Positive www state - passed")
	} else {
		t.Errorf("TestCheck|Test Positive www state - failed")
	}
	if !check(testFoundURLs, "https://mondo.com"){
		fmt.Println("TestCheck|Test Negative state - passed")
	} else {
		t.Errorf("TestCheck|Test Negative state - failed")
	}
	if check(testFoundURLs, ""){
		fmt.Println("TestCheck|Test Empty state - passed")
	} else {
		t.Errorf("TestCheck|Test Empty state - failed")
	}
	if check(testFoundURLs, "mailto:https://monzo.com"){
		fmt.Println("TestCheck|Test Non-url state - passed")
	} else {
		t.Errorf("TestCheck|Test Non-url state - failed")
	}
	testFoundURLs = []string{}
	if !check(testFoundURLs, "https://monzo.com"){
		fmt.Println("TestCheck|Test Empty found array state - passed")
	} else {
		t.Errorf("TestCheck|Test Empty found array state - failed")
	}
}

func TestFindAllLinks(t *testing.T) {
	urlPrefix = "https://monzo.com"
	mockPage := strings.NewReader(` 
		<p>
		<a href="http://monzo.com">1</a>
		<a href='www.mondo.com'>2</a>
		<a style=\"\" href=https://monzo.com/blog>3</a>
		<a href='/blog'>4</a>
		<a href='/about'>5</a>
		http://thisShouldNotBeFound.com
		</p>
	`)

	links := FindAllLinks(mockPage, urlPrefix)

	if len(links) != 4 {
		t.Errorf("TestFindAllLinks|Test Number of links returned - failed")
	} else {
		fmt.Println("TestFindAllLinks|Test Number of links returned - passed")
	}
	if links[0] != "http://monzo.com" {
		t.Error("TestFindAllLinks|Test First link - failed")
	} else {
		fmt.Println("TestFindAllLinks|Test First link - passed")
	}
	if links[1] != "www.mondo.com" {
		t.Error("TestFindAllLinks|Test Second link - failed")
	} else {
		fmt.Println("TestFindAllLinks|Test Second link - passed")
	}
	if links[2] != "https://monzo.com/blog" {
		t.Error("TestFindAllLinks|Test Third link - failed")
	} else {
		fmt.Println("TestFindAllLinks|Test Third link - passed")
	}
	if links[3] != "https://monzo.com/about" {
		t.Error("TestFindAllLinks|Test Fourth link - failed")
	} else {
		fmt.Println("TestFindAllLinks|Test Fourth link - passed")
	}

	mockPage2 := strings.NewReader(``)
	links = FindAllLinks(mockPage2, urlPrefix)
	if len(links) != 0 {
		t.Errorf("TestFindAllLinks|Test Empty page - failed")
	} else {
		fmt.Println("TestFindAllLinks|Test Empty page - passed")
	}
}

