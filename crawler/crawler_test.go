package crawler

import (
	"testing"
	"fmt"
)

func TestCrawlWebSingleLevel(t *testing.T) {
	if CrawlWeb("https://shields.io", true){
		fmt.Println("TestCrawl| Test Single Level - passed")
	} else {
		t.Errorf("TestCrawl| Test Single Level - failed")
	}
}

func TestCrawlWebMultiLevel(t *testing.T){
	if CrawlWeb("https://monzo.com", true){
		fmt.Println("TestCrawl| Test Multi Level - passed")
	} else {
		t.Errorf("TestCrawl| Test Multi Level - failed")
	}
}


func TestMakeWebsite(t *testing.T){
	testURL := "http://shields.io"
	testURLTwo := "http://gitignore.io"
	getWebsites = make(chan string)
	seenURLAlready = make(map[string]bool)
	websites = []website{}
	urlsToCheck = 2
	urlPrefix = "https://github.com"
	makeWebsite(testURL)
	if len(seenURLAlready) == 3 {
		fmt.Println("TestMakeWebsite|Test New URL - passed")
	} else {
		t.Errorf("TestMakeWebsite|Test New URL - failed")
	}
	makeWebsite(testURLTwo)
	if len(seenURLAlready) == 5 {
		fmt.Println("TestMakeWebsite|Test Second New URL - passed")
	} else {
		t.Errorf("TestMakeWebsite|Test Second New URL - failed")
	}
	makeWebsite(testURL)
	if len(seenURLAlready) == 5 {
		fmt.Println("TestMakeWebsite|Test Duplicate URL - passed")
	} else {
		t.Errorf("TestMakeWebsite|Test Duplicate URL - failed")
	}
}

func TestPrintWebsite(t *testing.T){
	printWebsite(website{url:"www.testNormal.co.uk", hrefs:[]string{"www.testhref1.com", "www.testhref2.com", "www.testhref3.com"}})
	printWebsite(website{url:"www.testMissingLink.co.uk", hrefs:[]string{"www.testhref1.com", "", "www.testhref2.com"}})
	printWebsite(website{url:"", hrefs:[]string{"www.shouldnotprint1.com"}})
	printWebsite(website{url:"www.testNoLinks.co.uk", hrefs:[]string{}})
}

func TestPrintSiteMapping(t *testing.T) {
	websites = []website{
		{url: "www.testNormal1.co.uk", hrefs: []string{"www.testhref1.com", "www.testhref2.com", "www.testhref3.com"}},
		{url: "www.testNormal2.co.uk", hrefs: []string{"www.testhref4.com", "www.testhref5.com", "www.testhref6.com"}},
		{url: "www.testNormal3.co.uk", hrefs: []string{"www.testhref7.com", "www.testhref8.com", "www.testhref9.com"}}}
	printSiteMapping()
}
