package crawler

import "testing"

func TestCreateSiteMap(t *testing.T) {
	websites = []website{
		{url: "www.testNormal1.co.uk", hrefs: []string{"www.testhref1.com", "www.testhref2.com", "www.testhref3.com"}},
		{url: "www.testNormal2.co.uk", hrefs: []string{"www.testhref4.com", "www.testhref5.com", "www.testhref6.com"}},
		{url: "www.testNormal3.co.uk", hrefs: []string{"www.testhref7.com", "www.testhref8.com", "www.testhref9.com"}},
		{url: "www.testNormal4.co.uk", hrefs: []string{"www.testhref10.com", "www.testhref11.com", "www.testhref12.com"}},
		{url: "www.testNormal5.co.uk", hrefs: []string{"www.testhref13.com", "www.testhref14.com", "www.testhref15.com"}},
		{url: "www.testNormal6.co.uk", hrefs: []string{"www.testhref16.com", "www.testhref17.com", "www.testhref18.com"}},
		{url: "www.testNormal7.co.uk", hrefs: []string{"www.testhref19.com", "www.testhref20.com", "www.testhref21.com",
		"www.testhref22.com", "www.testhref23.com", "www.testhref24.com","www.testhref25.com", "www.testhref26.com",
		"www.testhref27.com", "www.testhref28.com", "www.testhref29.com", "www.testhref30.com", "www.testhref31.com",
		"www.testhref32.com", "www.testhref33.com"}}}
	CreateSiteMap(websites, "../output/sitemap.html")
}


