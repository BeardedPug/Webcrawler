package crawler

import (
	"sync"
	"fmt"
	"net/http"
	"strings"
)

// Channel for urls to be checked
var getWebsites chan string
// Array of all the websites found
var websites []website
//Map for urls to be checked against if they are/have been in the queue
var seenURLAlready map[string]bool
//Base url to check against
var urlPrefix string
//How many urls in queue
var urlsToCheck int

var mux sync.Mutex

// A website has a url and contains a list of urls it links to.
type website struct {
	url string
	hrefs []string
}

/*
CrawlWeb
--------
This is the method that runs the web crawler. It puts the first url on the queue then continuously reads the queue,
asynchronously running makeWebsite() for each url in the queue. Once the queue is closed the crawl is deemed finished,
it prints the site-map to the console if printToConsole == true and then returns complete.
--------
Input  |
startUrl : The domain url string to search from and remain within.
printToConsole : bool to say whether the site-map should be printed to the console.
--------
Output |
complete : bool will return true if the crawl finishes correctly.
*/
func CrawlWeb(startUrl string, printToConsole bool) bool{

	getWebsites = make(chan string)
	websites = []website{}
	seenURLAlready = make(map[string]bool)
	urlPrefix = startUrl
	var complete bool

	go func() {getWebsites <- startUrl}()
	urlsToCheck = 1
	seenURLAlready[startUrl] = true

	fmt.Println("Starting crawl for: " + startUrl)

	for uri := range getWebsites {
		go makeWebsite(uri)
		fmt.Printf("\rProcessing %d links",urlsToCheck)
	}
	if printToConsole == true {
		fmt.Printf("\rSitemap for "+startUrl+";\n")
		printSiteMapping()
	} else {
		fmt.Printf("\rEnd of craw for: "+startUrl+"\n")
	}

	complete = true
	return complete
}

/*
makeWebsite
-----------
This method gets the body of the web page 'address'. Then it calls FindAllLinks with the body to receive back an array of
contained href's(urls). It creates a website object and adds it to the array of websites then loops through all the found
href's, if the href has not been seen before it is marked as seen and asynchronously added to the queue, to make a website
object for it, whilst it loops through the rest. Finally it checks if there are no more links to make website objects for,
and if so closes the queue.
-----------
Input     |
address : The url to make a website object of.
*/
func makeWebsite(address string) {
	if address != "" {
		response, err := http.Get(address)
		if err != nil {
			fmt.Print(err)
		} else {
			defer response.Body.Close()
			links := FindAllLinks(response.Body, urlPrefix)
			mux.Lock()
			websites = append(websites, website{url: address, hrefs:links})
			mux.Unlock()
			for linkNum := range links{
				link := links[linkNum]
				mux.Lock()
				done := seenURLAlready[link]
				mux.Unlock()
				if !done {
					if strings.HasPrefix(link, urlPrefix) {
						mux.Lock()
						seenURLAlready[link] = true
						mux.Unlock()
						urlsToCheck++
						go func() { getWebsites <- link }()
					}
				}
			}
			urlsToCheck--
			if urlsToCheck == 0 {
				close(getWebsites)
			}
			return
		}
	}
	mux.Lock()
	websites = append(websites, website{url: address, hrefs:[]string{}})
	mux.Unlock()
	urlsToCheck--
	if urlsToCheck == 0 {
		close(getWebsites)
	}
}

/* Method to loop through websites array and print each website to the console, in the given format;
page1:
        - link
        - ...
        - link

page2:
        - link
        - ...
        - link
etc...
 */
func printSiteMapping(){
	for website := range websites{
		printWebsite(websites[website])
	}
}

/* Method that for a given website will print in the given format;
page:
        - link1
        - link2
        - link3
etc...
-----------
Input     |
page : The website object to print the contents of.
 */
func printWebsite(page website) {
	if page.url != "" {
		fmt.Println(page.url + ":")
		for link := range page.hrefs {
			if page.hrefs[link] != "" {
				fmt.Println("        - " + page.hrefs[link])
			}
		}
	}
}
