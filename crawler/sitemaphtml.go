package crawler

import (
	"os"
	"bufio"
	"fmt"
	"strings"
)

/*
CreateSiteMap
-------------
A method to represent an array of website objects as a site-map with bootstrap html to persist the site-map and
allow it to be more readable and usable. The method creates a file with the given name then writes all the html to it,
encapsulating each website as part of its own 'div'. Each website can make up to 25% of the page in width and be a
maximum of 500px long, any overflow is dealt with by using scroll bars. This keeps the content neat and regular with
three columns of web-pages.
-------------
Input       |
pages : An array of website objects to make a site-map of.
outputFile : The file location and name to write the html to.
 */
func CreateSiteMap(pages []website, outputFile string){
	websiteURL := pages[0].url // Presumed to be the domain as the crawler reads this url first
	outputDir := outputFile[:strings.LastIndex(outputFile, "/")]
	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		os.Mkdir(outputDir, os.ModePerm)
	}
	file, err := os.Create(outputFile)
	errorCheck(err)
	defer file.Close()
	writer := bufio.NewWriter(file)
	toPrint := "<!doctype html>\n" +
		"<html lang='en'>\n" +
		"<head>\n<meta charset='utf-8'>\n" +
		"<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>\n"+
		"<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css' " +
		"integrity='sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB' crossorigin='anonymous'>\n"+
		"<title>Sitemap for "+websiteURL+"</title>\n"+
		"</head>\n"+
		"<body>\n"+
		"<div class='jumbotron text-center'>\n"+
		"<h1>Sitemap for "+websiteURL+"</h1>\n"+
		"</div>\n"+
		"<div class='container-fluid'>\n"
	_, err = fmt.Fprintf(writer, toPrint)
	errorCheck(err)
	for webpage := range pages{
		if webpage % 3 == 0{
			toPrint = "<div class='row'>\n"
			_, err = fmt.Fprintf(writer, toPrint)
			errorCheck(err)
		}
		page := pages[webpage]
		if page.url != "" {
			toPrint = "<div class='col-md-4 bg-light text-dark' style='max-width: 25%;'>\n"+
				"<hr>\n<h3><a href = "+page.url + ">"+page.url+"</a></h3>\n<hr>\n"+
				"<div style='overflow: scroll; text-overflow: ellipsis; max-height: 500px;'>\n"
			_, err = fmt.Fprintf(writer, toPrint)
			errorCheck(err)
			for link := range page.hrefs {
				if page.hrefs[link] != "" {
					toPrint = "<p>        - <a href = " + page.hrefs[link] + ">"+page.hrefs[link]+"</a><p>\n"
					_, err = fmt.Fprintf(writer, toPrint)
					errorCheck(err)
				}
			}
			_, err = fmt.Fprintf(writer, "</div>\n</div>\n")
			errorCheck(err)
		}
		if webpage % 3 == 2{
			toPrint = "</div>\n"
			_, err = fmt.Fprintf(writer, toPrint)
			errorCheck(err)
		}
	}
	toPrint =  "</div>\n"+
		"<script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>\n"+
		"<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script>\n"+
		"<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js' integrity='sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T' crossorigin='anonymous'></script>\n"+
		"</body>\n"+
		"</html>"
	_, err = fmt.Fprintf(writer, "</html>")
	errorCheck(err)
	writer.Flush()
}

//Given an outputFile location it uses the array of websites found by the crawler to produce an html representation of the site map
func CreateCrawlerMap(outputFile string){
	CreateSiteMap(websites, outputFile)
}

// Checks if an error occurred, if so prints it out to the console.
func errorCheck(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
