package main

import (
	"Webcrawler/crawler"
)

// Run webcrawler for monzo.com domain and create it's sitemap
func main() {
	crawler.CrawlWeb("https://monzo.com", false)
	crawler.CreateCrawlerMap("output/sitemap.html") //Uncomment for html site map
}
